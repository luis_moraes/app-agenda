/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Model.Conta;
import java.util.ArrayList;
import javax.swing.AbstractListModel;
/**
 *
 * @author lpmoraes
 */

public class JListModelContas extends AbstractListModel {   

    public ArrayList<Conta> dados = new ArrayList<Conta>();    
             
    public JListModelContas(ArrayList<Conta> contas) {
        this.dados = contas;        
    }
    
    @Override
    public int getSize() {        
        return this.dados.size();        
    }

    @Override
    public Object getElementAt(int index) {                
        return dados.get(index);        
    }
        
    public int getIdAt(int index) {                
        return dados.get(index).getId();        
    }

   
}
