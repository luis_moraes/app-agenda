/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author azimuth
 */
public class JavaConnect {
    
    public Connection conn;
    
    public static Connection ConnectDb(){
     Connection conn = null;   
    try{
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:../app-agenda/src/banco/dbagenda.db");
        //JOptionPane.showMessageDialog(null, "Conecxão Estabelecida");
        
    }
    catch (Exception e){
        JOptionPane.showMessageDialog(null, e);
        return null;
    }
    return conn;
    
    }
    
    public void desconecta(){
        try {
            JOptionPane.showMessageDialog(null, "Conecxão com Banco Encerrada");
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(JavaConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

