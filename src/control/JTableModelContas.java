/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Model.Conta;
import Model.Lancamentos;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.UIManager.get;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lpmoraes
 */
public class JTableModelContas extends AbstractTableModel {

    public ArrayList<Conta> dados = new ArrayList<Conta>();
    private String[] colunas = {"Banco", "Descrição", "Agência", "Conta", "Saldo"};

    public JTableModelContas(ArrayList<Conta> contas) {
        this.dados = contas;
    }

    public void addRow(Conta c) {
        this.dados.add(c);
        this.fireTableDataChanged();
    }

    public String getColumnName(int num) {
        return this.colunas[num];
    }

    @Override
    public int getRowCount() {
        return dados.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        switch (coluna) {
            case 0:
                return dados.get(linha).getNomeBanco();
            case 1:
                return dados.get(linha).getDescricao();
            case 2:
                return dados.get(linha).getNumeroAgencia();
            case 3:
                return dados.get(linha).getNumeroConta();
            case 4:
                NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
                return numberFormat.format(dados.get(linha).getSaldo());
        }
        return null;
    }

    //remover a linha passando o número da mesma
    public void removeRow(int linha) {
        this.dados.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }

    //retorna linha passando o número da linha
    public Conta getRow(int linha) {
        return this.dados.get(linha);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        System.out.println("lin:" + rowIndex + " col:" + columnIndex);

        switch (columnIndex) {
            case 1:
                if (aValue instanceof String) {
                    dados.get(rowIndex).setDescricao((String) aValue);
                    System.out.println(dados.get(rowIndex).getDescricao());
                }
                break;

            case 2:
                if (aValue instanceof String) {
                    dados.get(rowIndex).setNumeroAgencia((String) aValue);
                    System.out.println(dados.get(rowIndex).getNumeroAgencia());
                }
                break;

            case 3:
                if (aValue instanceof String) {
                    dados.get(rowIndex).setNumeroConta((String) aValue);
                    System.out.println(dados.get(rowIndex).getNumeroConta());
                }
                break;

            case 4:                
                if (aValue instanceof String) {
                    String valor = (String) aValue.toString();
                    System.out.println(valor);
                    valor = valor.replace(".", "");
                    valor = valor.replaceAll(",", ".");              
                    valor = valor.replaceAll("R$ ", "");                              
                    double saldo = Double.valueOf(valor);                    
                    dados.get(rowIndex).setSaldo(dados.get(rowIndex).getSaldo() + saldo);                                       
                }
                break;
                
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }
}
