/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Model.Lancamentos;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lpmoraes
 */

public class JTableModelLancamentos extends AbstractTableModel {   

    public ArrayList<Lancamentos> dados = new ArrayList<Lancamentos>();
    private String[] colunas = {"Favorecido", "Data Pagamaento", "Valor", "Status"};
             
    public JTableModelLancamentos(ArrayList<Lancamentos> lancamentos) {
        this.dados = lancamentos;        
    }

    public void addRow(Lancamentos c) {
        this.dados.add(c);
        this.fireTableDataChanged();
    }

    public String getColumnName(int num) {
        return this.colunas[num];
    }

    @Override
    public int getRowCount() {
        return dados.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        switch (coluna) {
            case 0:
                return dados.get(linha).getFavorecido();
            case 1:
                return dados.get(linha).getDataPagamento();
            case 2:                
                Double valor = dados.get(linha).getValor();
                NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
                return numberFormat.format(valor);                
                //return dados.get(linha).getValor();
            case 3:
                return dados.get(linha).getStatus();         
        }
        return null;
    }

    //remover a linha passando o número da mesma
    public void removeRow(int linha) {
        this.dados.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }

    //retorna linha passando o número da linha
    public Lancamentos getRow(int linha) {
        return this.dados.get(linha);
    }

   
}
