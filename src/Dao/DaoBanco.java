/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Banco;
import Model.Municipio;
import control.JavaConnect;
import java.awt.List;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author lpmoraes
 */
public class DaoBanco {

    Connection conn = null;
    ResultSet resultSet;
    PreparedStatement preparedStatement;
    public boolean resposta;   
    

    public DaoBanco() {
        this.conn = JavaConnect.ConnectDb();        
        //this.objMunicipio = new ArrayList<Municipio>();
        //this.municipio = new Municipio();
    }

    public void buscarTodos(JComboBox comboBox_Banco) throws SQLException {
        String sql = "SELECT * FROM banco";
        try {
            this.preparedStatement = conn.prepareStatement(sql);
            this.resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Banco banco = new Banco();                
                banco.setId(resultSet.getInt("idBanco"));
                banco.setNome(resultSet.getString("nome"));                                
                comboBox_Banco.addItem(banco);                       
            }
        } catch (Exception e) {
            this.resultSet.close();
            this.preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        this.resultSet.close();
        this.preparedStatement.close();
        return;
    }        
    
    
    public Banco porId(int id) throws SQLException {              
        Banco banco = new Banco();
        String sql = "SELECT * FROM banco WHERE idBanco = ?";        
        try {             
            this.preparedStatement = conn.prepareStatement(sql);
            this.preparedStatement.setInt(1, id);                        
            this.resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                               
                banco.setId(resultSet.getInt("idBanco"));
                banco.setNome(resultSet.getString("nome"));                                                               
                banco.setAtivo(resultSet.getInt("ativo"));                
            }
        } catch (Exception e) {
            this.resultSet.close();
            this.preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        this.resultSet.close();
        this.preparedStatement.close();
        return banco;
    }
    
    
}
