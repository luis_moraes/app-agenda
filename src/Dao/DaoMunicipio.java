/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Municipio;
import control.JavaConnect;
import java.awt.List;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author lpmoraes
 */
public class DaoMunicipio {

    Connection conn = null;
    ResultSet resultSet;
    PreparedStatement preparedStatement;
    public boolean resposta;
    ArrayList<Municipio> municipios = new ArrayList<Municipio>();
    
    JComboBox combo;

    public DaoMunicipio() {
        this.conn = JavaConnect.ConnectDb();        
        //this.objMunicipio = new ArrayList<Municipio>();
        //this.municipio = new Municipio();
    }

    public void buscarTodos(JComboBox comboBox_Municipio) throws SQLException {
        String sql = "select * from municipio";
        try {
            this.preparedStatement = conn.prepareStatement(sql);
            this.resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Municipio municipio = new Municipio();                
                municipio.setId(resultSet.getInt("idMunicipio"));
                municipio.setNome(resultSet.getString("nome"));                                
                comboBox_Municipio.addItem(municipio);                       
            }
        } catch (Exception e) {
            this.resultSet.close();
            this.preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        this.resultSet.close();
        this.preparedStatement.close();
        return;
    }        
}
