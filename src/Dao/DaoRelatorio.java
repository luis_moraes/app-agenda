/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Lancamentos;
import Model.Relatorio;
import control.JavaConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author lpmoraes
 */
public class DaoRelatorio {

    Connection conn;
    PreparedStatement preparedStatement;
    ResultSet resultSet;
    ArrayList<Relatorio> relatorios;

    public DaoRelatorio() {
        conn = JavaConnect.ConnectDb();
    }

    public ArrayList<Relatorio> mesAtual() throws SQLException {
        relatorios = new ArrayList<Relatorio>();
        
        String sql = "SELECT c.descricao, l.valor, p.data_cadastro, f.nome, l.numero_agencia, l.numero_conta"
                + " FROM conta c INNER JOIN pagamento p ON c.idConta = p.idConta"
                + " INNER JOIN lancamento l ON l.idLancamento = p.idLancamento"
                + " INNER JOIN favorecido f ON f.idFavorecido = l.idFavorecido"
                + " WHERE c.ativo = 1";

        try {
            preparedStatement = conn.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Relatorio relatorio = new Relatorio();
                
                relatorio.setDescricao(resultSet.getString("descricao"));
                relatorio.setValor(resultSet.getDouble("valor"));
                relatorio.setDataCadastro(resultSet.getString("data_cadastro"));
                relatorio.setFavorecido(resultSet.getString("nome"));
                relatorio.setNumeroAgencia(resultSet.getString("numero_agencia"));                
                relatorio.setNumeroConta(resultSet.getString("numero_conta"));
                
                relatorios.add(relatorio);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoRelatorio.class.getName()).log(Level.SEVERE, null, ex);            
            JOptionPane.showMessageDialog(null, ex);
            resultSet.close();
            preparedStatement.close();
        }
        
         preparedStatement.close();
        resultSet.close();
        return relatorios;                
    }

}
