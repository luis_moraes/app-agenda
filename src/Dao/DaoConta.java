/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Banco;
import Model.Conta;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import control.JavaConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author lpmoraes
 */
public class DaoConta {

    DaoBanco daoBanco = new DaoBanco();
    Connection conn = null;
    ResultSet resultSet;
    PreparedStatement preparedStatement;
    public boolean resposta;
    Date date;
    DateFormat dateFormat;

    public DaoConta() {
        conn = JavaConnect.ConnectDb();
        this.conn = JavaConnect.ConnectDb();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = new Date();
        //this.objMunicipio = new ArrayList<Municipio>();
        //this.municipio = new Municipio();
    }

    public Conta porId(int idConta) throws SQLException {
        Conta conta = new Conta();
        String sql = "SELECT * FROM conta WHERE idConta = ? AND ativo = 1";
        try {

            this.preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, idConta);

            this.resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                conta.setId(resultSet.getInt("idConta"));
                conta.setIdBanco(resultSet.getInt("idBanco"));
                conta.setNumeroConta(resultSet.getString("numero_conta"));
                conta.setNumeroAgencia(resultSet.getString("numero_agencia"));
                conta.setDescricao(resultSet.getString("descricao"));
                conta.setAtivo(resultSet.getInt("ativo"));
                conta.setDataCadastro(resultSet.getString("data_cadastro"));
                conta.setSaldo(resultSet.getDouble("saldo"));
            }
        } catch (Exception e) {
            this.resultSet.close();
            this.preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);            
        }
        this.resultSet.close();
        this.preparedStatement.close();
        return conta;
    }

    public ArrayList<Conta> buscarTodas() throws SQLException {

        ArrayList<Conta> contas = new ArrayList<Conta>();

        String sql = "SELECT * FROM conta WHERE ativo = 1";
        try {
            //idBanco numero_conta numero_agencia descricao ativo data_cadastro
            this.preparedStatement = conn.prepareStatement(sql);
            this.resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Conta conta = new Conta();

                conta.setId(resultSet.getInt("idConta"));
                conta.setIdBanco(resultSet.getInt("idBanco"));
                conta.setNumeroConta(resultSet.getString("numero_conta"));
                conta.setNumeroAgencia(resultSet.getString("numero_agencia"));
                conta.setDescricao(resultSet.getString("descricao"));
                conta.setAtivo(resultSet.getInt("ativo"));
                conta.setDataCadastro(resultSet.getString("data_cadastro"));
                conta.setSaldo(resultSet.getDouble("saldo"));
                contas.add(conta);
            }
        } catch (Exception e) {
            this.resultSet.close();
            this.preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        this.resultSet.close();
        this.preparedStatement.close();
        return contas;
    }

    public ArrayList<Conta> carregarJTableContas() throws SQLException {

        ArrayList<Conta> contas = new ArrayList<Conta>();

        String sql = "SELECT * FROM conta WHERE ativo = 1";

        try {
            this.preparedStatement = conn.prepareStatement(sql);
            this.resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Conta conta = new Conta();
                Banco banco = new Banco();

                int idBanco = resultSet.getInt("idBanco");
                banco = daoBanco.porId(idBanco);

                conta.setId(resultSet.getInt("idConta"));
                conta.setNomeBanco(banco.getNome());
                conta.setDescricao(resultSet.getString("descricao"));
                conta.setNumeroAgencia(resultSet.getString("numero_agencia"));
                conta.setNumeroConta(resultSet.getString("numero_conta"));
                conta.setSaldo(resultSet.getDouble("saldo"));

                contas.add(conta);
            }
        } catch (Exception e) {
            this.resultSet.close();
            this.preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }

        this.resultSet.close();

        this.preparedStatement.close();
        return contas;
    }

    public void salvarConta(int idBanco, String conta, String agencia, String descricao, String saldo) throws SQLException {

        String str_data = dateFormat.format(date);

        String sql = "INSERT INTO conta"
                + " (idConta, idBanco, numero_conta, numero_agencia, descricao, saldo, ativo, data_cadastro)"
                + " VALUES (?,?,?,?,?,?,?,?)";

        try {

            saldo = saldo.replace(".", "");
            saldo = saldo.replaceAll(",", ".");
            double value = Double.parseDouble(saldo);

            //  int dia = Integer.parseInt(dia_pagamento);
            preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(2, idBanco);
            preparedStatement.setString(3, conta);
            preparedStatement.setString(4, agencia);
            preparedStatement.setString(5, descricao);
            preparedStatement.setDouble(6, value);
            preparedStatement.setInt(7, 1);
            preparedStatement.setString(8, str_data);

            preparedStatement.execute();
            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                JOptionPane.showMessageDialog(null, "Cadastro realizado com sucesso!");
            } else {
                JOptionPane.showMessageDialog(null, "Erro ao cadastradar");
            }
        } catch (Exception e) {
            resultSet.close();
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        resultSet.close();
        preparedStatement.close();

    }

    public void editar(int idConta, String desc, String numA, String numC, Double saldo) throws SQLException {

        try {
            String sql = "UPDATE conta SET descricao =?, numero_agencia =?, numero_conta =?, saldo =?"
                    + "  WHERE idConta=\"" + idConta + "\"";

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, desc);
            preparedStatement.setString(2, numA);
            preparedStatement.setString(3, numC);
            preparedStatement.setDouble(4, saldo);
            preparedStatement.executeUpdate();
//            if (resultSet.next()) {
            JOptionPane.showMessageDialog(null, "Edição realizada com sucesso!");
//                JOptionPane.showMessageDialog(null, "Cadastro realizado com sucesso!");
//            } else {
//                JOptionPane.showMessageDialog(null, "Erro ao cadastradar");
//            }
        } catch (Exception e) {
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        resultSet.close();
        preparedStatement.close();
    }

    public void desativarConta(int idConta) throws SQLException {

        try {
            String sql = "UPDATE conta SET ativo = 0 WHERE idConta=\"" + idConta + "\" AND ativo = 1";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Conta excluída com sucesso!");
        } catch (Exception e) {
            resultSet.close();
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        resultSet.close();
        preparedStatement.close();
    }

    public void debitarConta(int idConta, double novoSaldo) throws SQLException {
        try {
            String sql = "UPDATE conta SET saldo = \"" + novoSaldo + "\" WHERE idConta=\"" + idConta + "\" AND ativo = 1";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            resultSet.close();
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        resultSet.close();
        preparedStatement.close();
    }

}
