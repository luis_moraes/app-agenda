/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Lancamentos;
import Model.Municipio;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import control.JavaConnect;
//import control.lancamentosJTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import view.*;

/**
 *
 * @author azimuth
 */
public class DaoLancamento {

    Connection conn;
    DateFormat dateFormat;
    Date date;
    PreparedStatement preparedStatement;
    ResultSet resultSet;
    ArrayList<Lancamentos> lancamentos = new ArrayList<Lancamentos>();

    public DaoLancamento() {
        conn = JavaConnect.ConnectDb();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = new Date();
    }

    public Lancamentos porId(int idLancamento, int idMunicipio) throws SQLException {

        Lancamentos consulta = new Lancamentos();
        String sql = "SELECT * FROM lancamento WHERE idLancamento = ? AND idMunicipio = ? AND ativo = 1";

        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, idLancamento);
            preparedStatement.setInt(2, idMunicipio);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                consulta.setIdLancamento(resultSet.getInt("idLancamento"));
                consulta.setIdFavorecido(resultSet.getInt("idFavorecido"));
                consulta.setIdMunicipio(resultSet.getInt("idMunicipio"));
                consulta.setDiaPagamento(resultSet.getInt("dia_pagamento"));
                consulta.setValor(resultSet.getDouble("valor"));
                consulta.setAtivo(resultSet.getInt("ativo"));
                consulta.setDataCadastro(resultSet.getString("data_cadastro"));
            }
        } catch (Exception e) {
            resultSet.close();
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        preparedStatement.close();
        resultSet.close();
        return consulta;
    }

    public void salvar(int idFavorecido, int idMunicipio, String agencia, String conta, String valor, String dia_pagamento, String observacao) throws SQLException {
        String str_data = dateFormat.format(date);
        String sql = "INSERT INTO lancamento"
                + " (idLancamento, idFavorecido, idMunicipio, numero_agencia, numero_conta, dia_pagamento, valor, observacao, ativo, data_cadastro)"
                + " VALUES(?,?,?,?,?,?,?,?,?,?)";
        try {
            valor = valor.replace(".", "");
            valor = valor.replaceAll(",", ".");
            double value = Double.parseDouble(valor);

            int dia = Integer.parseInt(dia_pagamento);

            preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(2, idFavorecido);
            preparedStatement.setInt(3, idMunicipio);
            preparedStatement.setString(4, agencia);
            preparedStatement.setString(5, conta);
            preparedStatement.setInt(6, dia);
            preparedStatement.setDouble(7, value);
            preparedStatement.setString(8, observacao);
            preparedStatement.setInt(9, 1);
            preparedStatement.setString(10, str_data);

            preparedStatement.execute();
            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                JOptionPane.showMessageDialog(null, "Cadastro realizado com sucesso!");
            } else {
                JOptionPane.showMessageDialog(null, "Erro ao cadastradar");
            }
        } catch (Exception e) {
            resultSet.close();
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        resultSet.close();
        preparedStatement.close();
    }

    public String formatarData(int data) {
        String str = String.valueOf(data);
        if (str.length() == 1) {
            return ("0" + str);
        } else {
            return str;
        }
    }

    public ArrayList<Lancamentos> filtrarPorMes(int idMunicipio, int mes, int ano) throws SQLException {
        String str_mes = formatarData(mes);

        String sql = "SELECT l.idLancamento AS idLancamento, f.nome AS favorecido, l.dia_pagamento AS dia, l.valor AS valor,"
                + " CASE WHEN (SELECT 1 FROM pagamento p  WHERE p.mes_pagamento =  ?"
                + " AND p.ano_pagamento = ? AND l.idLancamento = p.idLancamento)"
                + " THEN 'PAGO' ELSE 'Ativo' END AS 'status'"
                + " FROM  favorecido  AS f JOIN lancamento AS l ON f.idFavorecido = l.idFavorecido"
                + " WHERE l.idMunicipio = ?"
                + " AND substr(l.data_cadastro, 1, 7) <= '" + ano + "-" + str_mes + "'"
                +" ORDER BY dia_pagamento";

        preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, mes);
        preparedStatement.setInt(2, ano);
        preparedStatement.setInt(3, idMunicipio);
        resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Lancamentos consulta = new Lancamentos();

            consulta.setIdLancamento(resultSet.getInt("idLancamento"));
            consulta.setFavorecido(resultSet.getString("favorecido"));
            consulta.setDiaPagamento(resultSet.getInt("dia"));
            consulta.setValor(resultSet.getDouble("valor"));
            consulta.setStatus(resultSet.getString("status"));

            String str_dia = formatarData(resultSet.getInt("dia"));
            String str_data = str_dia + "/" + str_mes + "/" + ano;
            consulta.setDataPagamento(str_data);

            lancamentos.add(consulta);
        }
        preparedStatement.close();
        resultSet.close();
        return lancamentos;
    }

    public ArrayList<Lancamentos> carregarTodos(int idMunicipio, int dia, int mes, int ano) throws SQLException {
        String str_mes = formatarData(mes);
        String str_dia = formatarData(dia);

        String sql = "SELECT l.idLancamento AS idLancamento, f.nome AS favorecido, l.dia_pagamento AS dia, l.valor AS valor,"
                + " CASE WHEN (SELECT 1 FROM pagamento p  WHERE p.mes_pagamento =  ?"
                + " AND p.ano_pagamento = ? AND l.idLancamento = p.idLancamento)"
                + " THEN 'PAGO' ELSE 'Ativo' END AS 'status' "
                + " FROM  favorecido  AS f JOIN lancamento AS l ON f.idFavorecido = l.idFavorecido "
                + " WHERE l.dia_pagamento = ? AND l.idMunicipio = ?"
                + " AND substr(l.data_cadastro, 1, 7) <= '" + ano + "-" + str_mes + "'";

        System.out.println("date('" + ano + "-" + str_mes + "-" + str_dia + "')");
        preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, mes);
        preparedStatement.setInt(2, ano);
        preparedStatement.setInt(3, dia);
        preparedStatement.setInt(4, idMunicipio);
        resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Lancamentos consulta = new Lancamentos();

            consulta.setIdLancamento(resultSet.getInt("idLancamento"));
            consulta.setFavorecido(resultSet.getString("favorecido"));
            consulta.setDiaPagamento(resultSet.getInt("dia"));
            consulta.setValor(resultSet.getDouble("valor"));
            consulta.setStatus(resultSet.getString("status"));

            String str_data = str_dia + "/" + str_mes + "/" + ano;

            consulta.setDataPagamento(str_data);

            lancamentos.add(consulta);
        }

        preparedStatement.close();
        resultSet.close();
        return lancamentos;
    }
    /* 
    
     1)
     SELECT f.nome AS favorecido, l.dia_pagamento AS dia, l.valor AS valor
     FROM  favorecido  AS f JOIN lancamento AS l ON f.idFavorecido = l.idFavorecido;
    
     2)
     SELECT l.idLancamento AS idLancamento, f.nome AS favorecido, l.dia_pagamento AS dia, l.valor AS valor,
     CASE WHEN (SELECT 1 FROM pagamento p WHERE p.mes_pagamento = 4 AND l.idLancamento = p.idLancamento ) THEN 'pago' ELSE 'ativo' END AS 'status' 
     FROM  favorecido  AS f JOIN lancamento AS l ON f.idFavorecido = l.idFavorecido;
   
     3)
     "SELECT l.idLancamento AS idLancamento, f.nome AS favorecido, l.dia_pagamento AS dia, l.valor AS valor,"
     + " CASE WHEN (SELECT 1 FROM pagamento p WHERE p.mes_pagamento = 4 AND l.idLancamento = p.idLancamento )"
     + " THEN 'PAGO' ELSE 'Ativo' END AS 'status'"
     + " FROM  favorecido  AS f JOIN lancamento AS l ON f.idFavorecido = l.idFavorecido"
     + " WHERE l.idMunicipio = ?"; 
    
     4)  
     SELECT l.idLancamento AS idLancamento, f.nome AS favorecido, l.dia_pagamento AS dia, l.valor AS valor,
     CASE WHEN (SELECT 1 FROM pagamento p  WHERE p.mes_pagamento =  2
     AND p.ano_pagamento =  2016
     AND l.idLancamento = p.idLancamento )
     THEN 'PAGO' ELSE 'Ativo' END AS 'status' 
     FROM  favorecido  AS f JOIN lancamento AS l ON f.idFavorecido = l.idFavorecido
     WHERE l.dia_pagamento  = 12
     AND l.idMunicipio = 1;
    
    
    
     String sql2 = "SELECT l.idLancamento AS idLancamento, f.nome AS favorecido, l.dia_pagamento AS dia, l.valor AS valor,"
     + " CASE WHEN (SELECT 1 FROM pagamento p WHERE p.mes_pagamento = 4 AND l.idLancamento = p.idLancamento )"
     + " THEN 'PAGO' ELSE 'Ativo' END AS 'status'"
     + " FROM  favorecido  AS f JOIN lancamento AS l ON f.idFavorecido = l.idFavorecido"
     + " WHERE l.idMunicipio = ?";       
    
    
     select  substr(data_cadastro, 4,2), substr(data_cadastro, 7,4)  from lancamento 
     WHERE  substr(data_cadastro, 4,2) >= '04' AND substr(data_cadastro, 7,4) <= '2017';
    
     select sum(valor) from lancamento where idLancamento >= 2 AND idLancamento < 5;
    
    
    
    
     update sqlite_sequence set seq = 1 where name='conta';
    
     */
}
