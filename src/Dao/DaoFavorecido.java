/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import control.JavaConnect;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author azimuth
 */
public class DaoFavorecido {

    Connection conn;
    ResultSet resultSet;
    PreparedStatement preparedStatement;    
    DateFormat dateFormat;   
    Date date; 
    
    int primaryKey;

    public DaoFavorecido() {
        conn = JavaConnect.ConnectDb();        
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = new Date();  
    }       
      
    public int salvar(String nome, String num_documento) throws SQLException {
        
        String str_data  = dateFormat.format(date);                        
        
        String sql = "INSERT INTO favorecido (idFavorecido, nome, numero_documento, ativo, data_cadastro) "
                +" VALUES(?,?,?,?,?)";                                
        //+" VALUES(null, '" + nome + "', '" + num_documento + ", 1, '" +str_data+ "' )";                                        
        try {            
            preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);            
            preparedStatement.setString(2, nome);
            preparedStatement.setString(3, num_documento);
            preparedStatement.setInt(4, 1);
            preparedStatement.setString(5, str_data);                        
            preparedStatement.execute();            
            resultSet = preparedStatement.getGeneratedKeys();            
            
            if (resultSet.next()) {                
                this.primaryKey = resultSet.getInt(1);
            } else {
                JOptionPane.showMessageDialog(null, "Erro ao cadastradar");
            }
        } catch (Exception e) {
            resultSet.close();
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        resultSet.close();
        preparedStatement.close();
        return this.primaryKey;
    }
    
    public void excluir(int idLancamento) throws SQLException {

        String sql = "DELETE FROM favorecido WHERE idFavorecido = \"" +idLancamento+ "\"";
        
        try {
                        
            preparedStatement = conn.prepareStatement(sql);            
            preparedStatement.executeUpdate();                        
        
        } catch (Exception e) {
           preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        
        preparedStatement.close();
    }
    
    
    //     public void removePessoa(String nome) {
//        try {
//            this.stm = this.conn.createStatement();
//            this.stm.executeUpdate("DELETE FROM pessoas WHERE nome = \"" + nome + "\"");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
}
