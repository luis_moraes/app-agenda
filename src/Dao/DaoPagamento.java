/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Lancamentos;
import control.JavaConnect;
//import control.lancamentosJTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author azimuth
 */
public class DaoPagamento {

    Connection conn;
    DateFormat dateFormat;
    Date date;
    int primaryKey;
    Calendar calendar;
    PreparedStatement preparedStatement;
    ResultSet resultSet;

    public DaoPagamento() {
        conn = JavaConnect.ConnectDb();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = new Date();
        calendar = Calendar.getInstance();
        calendar.setTime(date);
    }

    public int salvar(int idLancamento, int idConta) throws SQLException {

        String str_data = dateFormat.format(date);

        calendar.setTime(date); // pega a data atual do sistema
        int diaAtual = calendar.get(Calendar.DAY_OF_MONTH); // DIA
        int mesAtual = calendar.get(Calendar.MONTH); // MES
        mesAtual += 1;
        int anoAtual = calendar.get(Calendar.YEAR);  // ANO                               

        String sql = "INSERT INTO pagamento"
                + " (idPagamento, idLancamento, idConta, dia_pagamento, mes_pagamento, ano_pagamento, ativo, data_cadastro)"
                + " VALUES(?,?,?,?,?,?,?,?)";
        try {
            preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(2, idLancamento);
            preparedStatement.setInt(3, idConta);

            preparedStatement.setInt(4, diaAtual);
            preparedStatement.setInt(5, mesAtual);
            preparedStatement.setInt(6, anoAtual);

            preparedStatement.setInt(7, 1);
            preparedStatement.setString(8, str_data);

            preparedStatement.execute();
            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                this.primaryKey = resultSet.getInt(1);
                //JOptionPane.showMessageDialog(null, "Pagamento realizado com sucesso!");
            } else {
                JOptionPane.showMessageDialog(null, "Erro ao pagar");
            }
        } catch (Exception e) {
            resultSet.close();
            preparedStatement.close();
            JOptionPane.showMessageDialog(null, e);
        }
        resultSet.close();
        preparedStatement.close();
        return this.primaryKey;
    }
                                            

    public boolean favorecidoPago(int idLancamento, int mes) throws SQLException {

        String sql = "SELECT * FROM pagamento WHERE idLancamento = ? AND mes_pagamento = ?";
        
        preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, idLancamento);
        preparedStatement.setInt(2, mes);
        resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {            
            preparedStatement.close();
            resultSet.close();
            return true;          
        } else {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

    }

}
