/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import control.JavaConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author azimuth
 */
public class DaoUsuario {

    Connection conn = null;
    ResultSet resultSet;
    PreparedStatement preparedStatement;
    public boolean resposta;

    public DaoUsuario() {
        conn = JavaConnect.ConnectDb();
    }

    public boolean VerificaLogin(String usuario, String senha) throws SQLException {
        String sql = "select * from usuario where usuario ='" + usuario + "' and senha = '" + senha + "'";

        try {
            preparedStatement = conn.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                //JOptionPane.showMessageDialog(null, "OK!"); 
                this.resposta = true;
            } else {
                JOptionPane.showMessageDialog(null, "Usuário não cadastrado");
                this.resposta = false;
            }
        } catch (Exception e) {
            preparedStatement.close();
            resultSet.close();
            JOptionPane.showMessageDialog(null, e);

        }
        preparedStatement.close();
        resultSet.close();
        return this.resposta;
    }

}
