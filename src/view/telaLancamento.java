package view;

import control.MonetarioDocument;
import Dao.DaoBanco;
import Dao.DaoFavorecido;
import Dao.DaoLancamento;
import Dao.DaoMunicipio;
import Model.Banco;
import Model.Lancamentos;
import Model.Municipio;
import control.JTableModelLancamentos;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author azimuth
 */
public class telaLancamento extends javax.swing.JFrame {

    /**
     * Creates new form tcpfelaLancamento
     */
    int idMunicipio;
    DaoLancamento lanc;
    DaoFavorecido favorecido;
    DaoBanco daoBanco;
    DaoLancamento daoLancamento;
    JTableModelLancamentos jTableModelLanc;
    JTable jTable_Lancamentos;
    int dia, mes, ano;

    public telaLancamento(int id, JTable jTable_Lancamentos, int dia, int mes, int ano) throws ParseException {
        this.jTable_Lancamentos = jTable_Lancamentos;
        initComponents();
        this.idMunicipio = id;
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        buttonGroup2.add(rbCpf);
        buttonGroup2.add(rbCnpj);
        setLocationRelativeTo(null);
        lanc = new DaoLancamento();
        favorecido = new DaoFavorecido();
        daoBanco = new DaoBanco();
        carregarBancos(); 
        ftf_valor.setDocument(new MonetarioDocument());
        ftf_valor.setText("0");
    }

    public void carregarBancos() {
        comboBox_Banco.removeAll();

        try {
            this.daoBanco.buscarTodos(comboBox_Banco);
            //municipios = this.municipio.buscarTodos();                                           
        } catch (SQLException ex) {
            Logger.getLogger(telaLancamento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizarLancamentos() throws SQLException {
        this.daoLancamento = new DaoLancamento();
        ArrayList<Lancamentos> lancamentos;
        
        if (telaInicio.tipoLancamento == 1) {
            lancamentos = daoLancamento.carregarTodos(this.idMunicipio, this.dia, this.mes, this.ano);
        } else {//telaInicio.tipoLancamento == 2
            lancamentos = daoLancamento.filtrarPorMes(idMunicipio, telaInicio.mesFiltro, telaInicio.anoFiltro);
        }
        telaInicio.jTableModelLanc = new JTableModelLancamentos(lancamentos);
        this.jTable_Lancamentos.setModel(telaInicio.jTableModelLanc);
    }

    public boolean validarForm() {
        if (txtFavorecido.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Campo NOME é obrigatóio!", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
            //definir um limite de caracteres(no máximo 100)
        }

        if (ftfDocumento.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Campo CPF/CNPJ é obrigatóio!", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
        }

        if (txt_agencia.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Campo AGÊNCIA é obrigatóio!", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
            //colocar um mascara pra cada banco escolhindo
        }
        if (txt_conta.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Campo CONTA é obrigatóio!", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
            //colocar um mascara pra cada banco escolhindo
        }

        if (ftf_valor.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Campo VALOR é obrigatóio!", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
            //adicionar uma mascara de dinheiroS
        }

        if (formatText_DiaPagamento.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Campo DIA DE PAGEMENTO é obrigatóio!", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
            //tem que preencher os dois digitos(ex: quando digita só um 8 e não 08, dá erro)
            //tem que limitar até a última data do mes corrente
        }              
        if (formatText_DiaPagamento.getText().equals("__")) {
            JOptionPane.showMessageDialog(null, "Campo DIA DE PAGEMENTO tem que ter dois digitos. \n Exemplo: 01, 08, 22. ", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
            //tem que preencher os dois digitos(ex: quando digita só um 8 e não 08, dá erro)
            //tem que limitar até a última data do mes corrente
        }
        int dia =    Integer.valueOf(formatText_DiaPagamento.getText());
        if ( dia > 28 || dia < 1) {
            JOptionPane.showMessageDialog(null, "Escolher dia pagamento entre 1 a 28", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
            //tem que preencher os dois digitos(ex: quando digita só um 8 e não 08, dá erro)
            //tem que limitar até a última data do mes corrente
        }
        return true;
    }    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        label_diaPagamento = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jText_Observacao = new javax.swing.JTextArea();
        txtFavorecido = new javax.swing.JTextField();
        ftfDocumento = new javax.swing.JFormattedTextField();
        txt_conta = new javax.swing.JTextField();
        txt_agencia = new javax.swing.JTextField();
        comboBox_Banco = new javax.swing.JComboBox();
        ftf_valor = new javax.swing.JFormattedTextField();
        btnSalvar = new javax.swing.JButton();
        rbCpf = new javax.swing.JRadioButton();
        rbCnpj = new javax.swing.JRadioButton();
        formatText_DiaPagamento = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Nome do Favorecido");

        label_diaPagamento.setText("Dia de Pagamento");

        jLabel4.setText("Conta");

        jLabel5.setText("Agência");

        jLabel6.setText("Banco");

        jLabel7.setText("Valor");

        jLabel9.setText("Observação");

        jText_Observacao.setColumns(20);
        jText_Observacao.setRows(5);
        jScrollPane1.setViewportView(jText_Observacao);

        ftfDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ftfDocumentoActionPerformed(evt);
            }
        });

        comboBox_Banco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBox_BancoActionPerformed(evt);
            }
        });

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/document_open.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        rbCpf.setText("CPF");
        rbCpf.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbCpfMouseClicked(evt);
            }
        });
        rbCpf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbCpfActionPerformed(evt);
            }
        });

        rbCnpj.setText("CNPJ");
        rbCnpj.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbCnpjMouseClicked(evt);
            }
        });

        try {
            formatText_DiaPagamento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        formatText_DiaPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                formatText_DiaPagamentoActionPerformed(evt);
            }
        });
        formatText_DiaPagamento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                formatText_DiaPagamentoFocusGained(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(rbCpf)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rbCnpj))
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel7)
                                    .addComponent(label_diaPagamento))
                                .addGap(26, 26, 26)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ftf_valor, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_agencia, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_conta, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comboBox_Banco, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ftfDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(formatText_DiaPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFavorecido, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 45, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rbCpf)
                        .addComponent(rbCnpj))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtFavorecido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addComponent(ftfDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(comboBox_Banco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txt_conta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txt_agencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(ftf_valor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(formatText_DiaPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label_diaPagamento))
                .addGap(42, 42, 42)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ftfDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ftfDocumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ftfDocumentoActionPerformed

    private void rbCpfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbCpfActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbCpfActionPerformed

    private void rbCpfMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbCpfMouseClicked
        // TODO add your handling code here:
      //  maskDia();
        MaskFormatter mascCPF;
        try {
            mascCPF = new MaskFormatter("###.###.###-##");
            mascCPF.setValueContainsLiteralCharacters(false);
            mascCPF.setPlaceholderCharacter('_');
            DefaultFormatterFactory masccpf = new DefaultFormatterFactory(mascCPF);
            ftfDocumento.setFormatterFactory(masccpf);
            //ftfDocumento.setValue("");

        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_rbCpfMouseClicked

    private void rbCnpjMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbCnpjMouseClicked
        // TODO add your handling code here:
       // maskDia();
        MaskFormatter mascCNPJ;
        try {
            mascCNPJ = new MaskFormatter("##.###.###/####-##");
            mascCNPJ.setValueContainsLiteralCharacters(false);
            mascCNPJ.setPlaceholderCharacter('_');
            DefaultFormatterFactory masccnpj = new DefaultFormatterFactory(mascCNPJ);
            ftfDocumento.setFormatterFactory(masccnpj);

            //ftfDoc.setValue("");
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_rbCnpjMouseClicked

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        // TODO add your handling code here:
        
        String dia_pagamento = formatText_DiaPagamento.getText();
        String conta = txt_conta.getText();
        String agencia = txt_agencia.getText();
        String valor = ftf_valor.getText();  
        
       // ftf_valor.setText("0");
        
        
        String nome = txtFavorecido.getText();
        String num_documento = ftfDocumento.getText();
        String observacao = jText_Observacao.getText();

        Banco banco = new Banco();
        banco = (Banco) comboBox_Banco.getSelectedItem();
        int idBanco = banco.getId();
        
        System.out.println(valor);

        if (validarForm()) {
            try {
                int idFavorecido = favorecido.salvar(nome, num_documento);
                lanc.salvar(idFavorecido, this.idMunicipio, agencia, conta, valor, dia_pagamento, observacao);

                formatText_DiaPagamento.setText("");
                txt_conta.setText("");
                txt_agencia.setText("");
                ftf_valor.setText("");
                txtFavorecido.setText("");
                ftfDocumento.setText("");
                jText_Observacao.setText("");

                atualizarLancamentos();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar! sqlException:" + e);
            }
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void formatText_DiaPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_formatText_DiaPagamentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_formatText_DiaPagamentoActionPerformed

    private void comboBox_BancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBox_BancoActionPerformed

    }//GEN-LAST:event_comboBox_BancoActionPerformed

    private void formatText_DiaPagamentoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_formatText_DiaPagamentoFocusGained
        MaskFormatter mascDia;
        try {
            mascDia = new MaskFormatter("##");
            mascDia.setValueContainsLiteralCharacters(false);
            mascDia.setPlaceholderCharacter('_');
            DefaultFormatterFactory masc_dia = new DefaultFormatterFactory(mascDia);
            formatText_DiaPagamento.setFormatterFactory(masc_dia);
            //ftfDocumento.setValue("");

        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_formatText_DiaPagamentoFocusGained


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalvar;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox comboBox_Banco;
    private javax.swing.JFormattedTextField formatText_DiaPagamento;
    private javax.swing.JFormattedTextField ftfDocumento;
    private javax.swing.JFormattedTextField ftf_valor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jText_Observacao;
    private javax.swing.JLabel label_diaPagamento;
    private javax.swing.JRadioButton rbCnpj;
    private javax.swing.JRadioButton rbCpf;
    private javax.swing.JTextField txtFavorecido;
    private javax.swing.JTextField txt_agencia;
    private javax.swing.JTextField txt_conta;
    // End of variables declaration//GEN-END:variables
}
//       ftf_valor.setDocument(new MonetarioDocument());
//       ftf_valor.setText("0");       
        //ftf_valor.getDocument().addDocumentListener((DocumentListener) new MonetarioDocument());