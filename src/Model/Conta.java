/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Dao.DaoBanco;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lpmoraes
 */
public class Conta {

    DaoBanco daoBanco = new DaoBanco();

    int id, idBanco, ativo;
    String numero_conta, numero_agencia, descricao, data_cadastro, nomeBanco;
    Double saldo;
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setIdBanco(int idBanco) {
        this.idBanco = idBanco;
    }

    public int getIdBanco() {
        return this.idBanco;
    }

    public void setNumeroConta(String conta) {
        this.numero_conta = conta;
    }

    public String getNumeroConta() {
        return this.numero_conta;
    }

    public void setNumeroAgencia(String agencia) {
        this.numero_agencia = agencia;
    }

    public String getNumeroAgencia() {
        return this.numero_agencia;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }

    public int getAtivo() {
        return ativo;
    }

    public void setDataCadastro(String data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public String getDataCadastro() {
        return data_cadastro;
    }

    public void setNomeBanco(String nomeBanco) {
        this.nomeBanco = nomeBanco;
    }

    public String getNomeBanco() {
        return this.nomeBanco;
    }
    
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Double getSaldo() {
        return this.saldo;
    }

    @Override
    public String toString() {

        String str = null;
        try {
            str = getConsultaNomeBanco(this.idBanco) + " - " + this.descricao + " - " + this.numero_agencia + " - " + this.numero_conta;
        } catch (SQLException ex) {
            Logger.getLogger(Conta.class.getName()).log(Level.SEVERE, null, ex);
        }

        return str;
    }

    public String getConsultaNomeBanco(int id) throws SQLException {
        Banco banco = new Banco();
        banco = daoBanco.porId(id);        
        this.nomeBanco = banco.getNome();
        return this.nomeBanco;
    }

}
