/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lpmoraes
 */
public class Lancamentos {

    String favorecido, status, data_cadastro, dataPagamento;
    int dia_pagamento, idLancamento, idFavorecido, idMunicipio, ativo;
    double valor;

    public void setIdLancamento(int id) {
        this.idLancamento = id;
    }

    public int getIdLancamento() {
        return this.idLancamento;
    }

    public void setIdFavorecido(int idFavorecido) {
        this.idFavorecido = idFavorecido;
    }

    public int getIdFavorecido() {
        return this.idFavorecido;
    }

    public void setIdMunicipio(int idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public int getIdMunicipio() {
        return this.idMunicipio;
    }

    public void setDiaPagamento(int dia) {
        this.dia_pagamento = dia;
    }

    public int getDiaPagamento() {
        return this.dia_pagamento;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getValor() {
        return this.valor;
    }

    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }

    public int getAtivo() {
        return ativo;
    }

    public void setDataCadastro(String data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public String getDataCadastro() {
        return data_cadastro;
    }

    /* Esta coluna nao existe no banco. Este dois metodos sao usados para definir o status 
     do lancamento de acordo com a consulta carregaTodos() em Dao Lancamento */
    public void setFavorecido(String favorecido) {
        this.favorecido = favorecido;
    }

    public String getFavorecido() {
        return this.favorecido;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setDataPagamento(String dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public String getDataPagamento() {
        return this.dataPagamento;
    }
}
