/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lpmoraes
 */
public class Banco {

    int id, ativo;
    String nome, data_cadastro;
 
//    public Municipio(int id, String nome, int ativo, String data_cadastro) {
//        this.id = id;
//        this.ativo = ativo;
//        this.nome = nome;
//        this.data_cadastro = data_cadastro;
//    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }

    public int getAtivo() {
        return ativo;
    }

    public void setDataCadastro(String data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public String getDataCadastro() {
        return data_cadastro;
    }

     @Override
    public String toString() {
        return this.nome;
    }
}
