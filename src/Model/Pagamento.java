/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lpmoraes
 */
public class Pagamento {
     
    String data_cadastro;
    int  id, idLancamento, idConta, dia_pagamento, mes_pagamento, ano_pagamento, ativo;    
    
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
            
    public void setIdLancamento(int id) {
        this.idLancamento = id;
    }
    
    public int getIdLancamento() {
        return this.idLancamento;
    }
    
    
    public void setIdConta(int idConta) {
        this.idConta = idConta;
    }
    
    public int getIdidConta() {
        return this.idConta;
    }
    
    public void setDiaPagamento(int dia) {
        this.dia_pagamento = dia;
    }
    
    public int getDiaPagamento() {
        return this.dia_pagamento;
    }
    
    public void setMesPagamento(int mes) {
        this.mes_pagamento = mes;
    }
    
    public int getMesPagamento() {
        return this.mes_pagamento;
    }
    
    public void setAnoPagamento(int ano) {
        this.ano_pagamento = ano;
    }
    
    public int getAnoPagamento() {
        return this.ano_pagamento;
    }
    
    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }

    public int getAtivo() {
        return ativo;
    }

    public void setDataCadastro(String data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public String getDataCadastro() {
        return data_cadastro;
    }

}
