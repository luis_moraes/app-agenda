/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author lpmoraes
 */
public class Relatorio {

    String descricaoConta, data_cadastro, favorecido, numero_agencia, numero_conta;
    double valor;

    public void setDescricao(String descricaoConta) {
        this.descricaoConta = descricaoConta;
    }

    public String getDescricao() {
        return descricaoConta;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getValor() {
        return this.valor;
    }
    
    public void setFavorecido(String favorecido) {
        this.favorecido = favorecido;
    }

    public String getFavorecido() {
        return this.favorecido;
    }
    
    public void setNumeroConta(String conta) {
        this.numero_conta = conta;
    }

    public String getNumeroConta() {
        return this.numero_conta;
    }

    public void setNumeroAgencia(String agencia) {
        this.numero_agencia = agencia;
    }

    public String getNumeroAgencia() {
        return this.numero_agencia;
    }

    public void setDataCadastro(String data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public String getDataCadastro() {
        return data_cadastro;
    }

}
